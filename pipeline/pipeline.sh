#!/bin/bash
#SBATCH --ntasks=20
#SBATCH --mem-per-cpu=1G 

module load system/parallel-20180122

#Pipeline directory
g_dir="."

#########################################
#          MANAGE ARGUMENTS             #
#########################################

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    "--workdir") set -- "$@" "-w" ;;
    "--file") set -- "$@" "-f" ;;
    "--length") set -- "$@" "-l" ;;
    "--coverage") set -- "$@" "-c" ;;
    "--output-dir") set -- "$@" "-o" ;;
    "--rmna") set -- "$@" "-r" ;;
    "--natozero") set -- "$@" "-z" ;;
    "--absolute") set -- "$@" "-a" ;;
    "--diploid") set -- "$@" "-d" ;;
    "--minQ") set -- "$@" "-Q" ;;
    "--padR") set -- "$@" "-R" ;;
    "--mapdamage") set -- "$@" "-m" ;;
    "--fragmentlength") set -- "$@" "-L" ;;
    "--sequencing-error") set -- "$@" "-s" ;;
    "--platform") set -- "$@" "-p" ;;
    "--damage") set -- "$@" "-d" ;;
    "--diploidy") set -- "$@" "-D" ;;
    *)        set -- "$@" "$arg"
  esac
done

#program options
declare -a arr
output_dir="$g_dir"
minQ=20
R=0
rmNA=0
NaToZ=0
absolutePos=0
n=0.04
ploidy=1

while getopts "F:g:w:f:l:o:c:s:L:p:d:m:M:n:R:Q:Drza" option; do
    case "${option}" in
        #specify the pipeline directory
        g)g_dir=${OPTARG};;
        #specify the working directory (where temporary files will be stored)
        w)work_dir=${OPTARG};;
        #add distribution File
        F)arr+=("-f ${OPTARG}");;
        #Path to the simulation file
        f)simufile_path=${OPTARG};;
        #Length of the sequence to simulate
        l)seqlength=${OPTARG};;
        #specify output directory
        o)output_dir=${OPTARG};;
        #cover at the input of gargammel
        c)arr+=("-c ${OPTARG}");;
        #sequencing error rate
        s)arr+=("-qs ${OPTARG} -qs2 ${OPTARG}");;
        #specify fragment length        
        L)arr+=("-l ${OPTARG}");; #default 35
        #specify illumina platform
        p)arr+=("-ss ${OPTARG}");;
        #deamination simulation
        d)arr+=("-damage ${OPTARG}");;
        #deamination using mapDamage
        m)arr+=("-mapdamage ${OPTARG} single");;
        #Misincorporation
        M)arr+=("--misince ${OPTARG}");;
        #number of missmatch during the mapping
        n)n=${OPTARG};;
        #add R bases at the extrimities of the sequence to avoid mapping errors        
        R)R=${OPTARG};;
        #call variant filter (number of read to match position) 
        Q)minQ=${OPTARG};;
        #Specify that we will work on diploid individuals
        D)ploidy=2;;
        #rm columns containing missing values (pass to True)
        r)rmNA=1;;
        #switch missing values with zero (pass to True)
        z)NaToZ=1;;
        #use absolute positions (by default use relative)
        a)absolutePos=1;;
    esac
done

cd $g_dir
work_dir="$g_dir"/work

start=`date +%s`

#Specify the reference sequence for mapping and calling
#Corresponding to the random sequence generated in the python script
ref=$work_dir/reference/reference.fa

#generate fasta files from the simulation file and index the reference sequence
srun -N1 -n1 bash_script/pre_garga.sh "$g_dir" "$simufile_path" "$seqlength" "$ref" "$R" "$absolutePos" "$ploidy" "$work_dir"

end=`date +%s`
runtime=$((end-start))
echo "generate fasta time,$runtime">>"$g_dir"/time.txt

#for each individual (directory): use gargammel and apply post treatment in parallel
srun="srun --exclusive -N1 -n1"
parallel="parallel -j $SLURM_NTASKS"

find work/fastas -mindepth 1 -maxdepth 1| $parallel $srun bash_script/individual_Garga_and_postTreatment.sh "$g_dir" {} "$ref" "$minQ" "$n" "$ploidy" "$work_dir" "$NaToZ" "${arr[@]}"

end=`date +%s`
runtime=$((end-start))
echo "individuals treatment time,$runtime">>"$g_dir"/time.txt

#merge all individual vcf and create the corresponding matrix, produce npz file
srun -N1 -n1 bash_script/merge_and_create_npz.sh "$seqlength" "$simufile_path" "$R" "$rmNA" "$NaToZ" "$absolutePos" "$output_dir" "$work_dir"

end=`date +%s`
runtime=$((end-start))
echo "merge and filter time,$runtime">>"$g_dir"/time.txt

#remove temporary files
rm -r $work_dir