#!/bin/bash

module load system/Python-3.7.4
module load bioinfo/samtools-1.8
module load bioinfo/FastQC_v0.11.5
module load bioinfo/bwa-0.7.17

g_dir=$1
simufile_path=$2
seqlength=$3 
ref=$4
R=$5
relativePos=$6
ploidy=$7
work_dir=$8


#Buid directory tree
mkdir -p "$work_dir"
mkdir -p "$work_dir"/result
mkdir -p "$work_dir"/reference
mkdir -p "$work_dir"/fastas

#Use python script to translate the simulation data to a matrix of snp
#The repertory "reference" and "fastas" must be created to use this script
python3 py_script/npz_to_fasta.py $work_dir "$simufile_path" "$seqlength" "$R" "$relativePos" "$ploidy"

#Premap / index the reference sequence
bwa index -a bwtsw "$ref"
samtools faidx "$ref"