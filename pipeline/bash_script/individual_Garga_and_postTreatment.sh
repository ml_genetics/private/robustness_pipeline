#!/bin/bash

module load bioinfo/samtools-1.8
module load bioinfo/gargammel-4f50a6d
module load bioinfo/TrimGalore-0.6.5
module load bioinfo/cutadapt-2.1
module load bioinfo/FastQC_v0.11.5
module load bioinfo/bwa-0.7.17
module load bioinfo/tabix-0.2.5
module load bioinfo/vcftools-0.1.15
module load bioinfo/bcftools-1.9

g_dir=$1; shift
dir=$1; shift
ref=$1; shift
minQ=$1; shift
n=$1; shift
ploidy=$1; shift
work_dir=$1; shift
NaToZ=$1; shift

start=`date +%s`

#gagammel arguments
arr=("$@")

filename=$(basename -- "$dir")

gargammel.pl "${arr[@]}" "$dir"

end=`date +%s`
runtime=$((end-start))
echo "gargammel,$runtime">>"$g_dir"/timegarga.txt

###################
# Adapter removal #
###################
gunzip $dir/simadna_s1.fq.gz
gunzip $dir/simadna_s2.fq.gz

#Trim reads / remove adapter / remove reads having a size below 25 bp
trim_galore --paired --length 25 -o $dir $dir/simadna_s1.fq $dir/simadna_s2.fq

###########
# Mapping #
###########

# BWA assembly
bwa aln -n "$n" "$ref" $dir/simadna_s1_val_1.fq > $dir/reads.sai; bwa samse "$ref" $dir/reads.sai $dir/simadna_s1_val_1.fq > $dir/readset.sam
nd=`date +%s`
runtime=$((end-start))
echo "mapping,$runtime">>"$g_dir"/timegarga.txt


samtools view -S $dir/readset.sam -b -o $dir/readset.bam

# Cleanup
rm $dir/simadna*

# Sort and remove duplicates
samtools sort $dir/readset.bam -o $dir/readset_sort.bam
samtools rmdup -s $dir/readset_sort.bam $dir/readset_sort_nodup.bam

# Create index
samtools index $dir/readset_sort_nodup.bam

# Cleanup
rm $dir/readset.bam $dir/readset.sam $dir/readset_sort.bam

#################
# Call variants #
#################

#Generate text pileup output for BAM file
samtools mpileup -t AD,INFO/AD -B -v -f "$ref" $dir/readset_sort_nodup.bam |

#different call for different ploidy
if [ $ploidy -eq 1 ]
then
bcftools call --ploidy "$ploidy" -m -f GQ -O z - > $dir/variants.vcf.gz
else
# by default use diploid individuals 
bcftools call -m -f GQ -O z - > $dir/variants.vcf.gz
fi

nd=`date +%s`
runtime=$((end-start))
echo "callvariant,$runtime">>"$g_dir"/timegarga.txt

#minQ : Includes only sites with Quality value above this threshold, 20 by default

#If we convert NAToZero option keep only SNP sites (as we don't need NA informations which will be replace by 0)
#Also filter by bwa quality in all the cases
if [ "$NaToZ" = "1" ] 
then
vcftools --gzvcf $dir/variants.vcf.gz --minQ "$minQ" --remove-indels  --non-ref-ac-any 1 --recode --recode-INFO-all  \
		--stdout > $dir/variants_biallelic.vcf
else
vcftools --gzvcf $dir/variants.vcf.gz --minQ "$minQ" --remove-indels --recode --recode-INFO-all\
		--stdout > $dir/variants_biallelic.vcf
fi

bgzip -f $dir/variants_biallelic.vcf

mv $dir/variants_biallelic.vcf.gz $work_dir/result/$filename.vcf.gz

#Index vcf
tabix $work_dir/result/$filename.vcf.gz

#remove the working directory after the execution
rm -rf $dir