#!/bin/bash

module load system/Python-3.7.4
module load bioinfo/tabix-0.2.5
module load bioinfo/vcftools-0.1.15
module load bioinfo/bcftools-1.9

length=$1
simufile_path=$2
R=$3
rmNA=$4
NaToZ=$5
relativePos=$6
output_dir=$7
work_dir=$8

#######################################
# Merge all VCF and create SNP Matrix #
#######################################

#If Natozero option activated just merge end remove multiallelic sites (>2) (as we have only SNP Sites at this step)
#Else merge and keep only SNP Site (as we have all sites) and remove multiallelic sites
if [ "$NaToZ" = "0" ] 
then
#Create VCF with all individuals (merge all vcf)
bcftools merge --force-samples `ls "$work_dir"/result/*.vcf.gz | sort -V` > $work_dir/result/allSamples.vcf

#Remove non variant and multiple variant
vcftools --vcf $work_dir/result/allSamples.vcf --non-ref-ac-any 1 --recode --recode-INFO-all --stdout > $work_dir/result/allSamplesJustVariant.vcf

else
#Create VCF with all individuals (merge all vcf) NA as 0
bcftools merge --force-samples --missing-to-ref `ls "$work_dir"/result/*.vcf.gz | sort -V` > $work_dir/result/allSamples.vcf

#Remove multiple variant (more than 1 alternative)
vcftools --vcf $work_dir/result/allSamples.vcf --max-alleles 2 --recode --recode-INFO-all --stdout > $work_dir/result/allSamplesJustVariant.vcf

fi

#create the npz file / compute distance -> result in the file distance.txt
python3 py_script/vcf_to_npz.py "$length" $work_dir/result/allSamplesJustVariant.vcf "$simufile_path" "$R" "$rmNA" "$NaToZ" "$relativePos" "$output_dir"