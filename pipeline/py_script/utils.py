#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 10:05:17 2020

@author: mathieu
"""
import numpy as np
import json

def load_dict_from_json(filepath):
    """Load a json in a dictionnary.

    The dictionnary contains the overall parameters used for the simulation (e.g. path to the data folder, number of epoch). The json can be created from a dictionnary using ``save_dict_in_json``.

    Arguments:
        filepath (string): filepath to the json file
    """
    return json.loads(open(filepath).read())

def searchSNP(pos, simu, seq):
    """
    Create a dictionary containing the SNP positions.
    """
    #Complement and SNP dictionaries
    comp = {'A':['T','C','G'], 'T':['A','C','G'], 'G':['A','C','T'], 'C':['A','T','G']}
    snp = {'A': {'x':[], 'y':[]},
           'T': {'x':[], 'y':[]},
           'C': {'x':[], 'y':[]},
           'G': {'x':[], 'y':[]}}

    for i,ss in enumerate(simu.T):                              #For each column
        wre = np.where(ss)[0]                                     #Position where ss==1
        mut = np.random.choice(comp[seq[pos[i]]])            #Random mutation in site P[i]
        
        if pos[i] not in snp[mut]['y']:
            snp[mut]['x'] += wre.tolist()
            snp[mut]['y'] += [pos[i]] * len(wre)
    return snp

def read_ms_compressed(npzfile, key="all"):
    """
    Takes a .npz file and return all data (SNP and position).
    If one want to get only SNP matrix, set key="SNP",
    or key="POS" for only Position arrays.
    """
    data = np.load(npzfile)
    if key=="all":
        return data["SNP"], data["POS"]
    else:
        return data[key]

def concatenate_list(list):
    result= ''
    for element in list:
        result += str(element)
    return result
