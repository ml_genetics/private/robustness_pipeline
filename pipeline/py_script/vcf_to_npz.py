#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 14:50:58 2020

@author: mathieu
"""
import numpy as np
import allel as al
import distance as dis
import sys

if __name__ == '__main__':
	
	#arguments
	length = int(sys.argv[1])
	vcfName = sys.argv[2]
	originalData = sys.argv[3]
	R = int(sys.argv[4])
	rmNA = bool(int(sys.argv[5]))
	switchNA = bool(int(sys.argv[6]))
	absolutePos = bool(int(sys.argv[7]))
	output_dir = sys.argv[8]
	
	#read vcf
	try :
		 vcf = al.read_vcf(vcfName, fields = ['variants/POS', 'calldata/GT'])
		 gargaMatrix = vcf["calldata/GT"][:,:,0]
	except TypeError :
		print("ERROR : cannot rebuild the matrix, try with other arguments (better coverage ...)")
		sys.exit(1)
		
	#extact gargammel matrix and position
	gargaMatrix = np.swapaxes(gargaMatrix, 0,1)
	gargaPos = vcf["variants/POS"]
	
	#Remove extremities positions and corresponding columns if R > 0
	if R != 0:
		indFalsePos = np.where(np.logical_or(gargaPos <= R , gargaPos >= length + R))
		gargaPos = np.delete(gargaPos, indFalsePos[0])
		gargaMatrix = np.delete(gargaMatrix, indFalsePos[0], 1)
		gargaPos = gargaPos - R
	
	#index of a numpy array start at 0 while allel position start at 1
	gargaPos = gargaPos - 1
	
	#rearange position, to be between 0 and 1
	if not absolutePos:
		gargaPos = gargaPos / length
	
	#drop columns containing na values
	if rmNA :
		indColumnRemove = []
		for n in range(gargaMatrix.shape[1]):
			if np.isin(-1, gargaMatrix[:,n]):
				indColumnRemove.append(n)
		#drop column
		gargaMatrix = np.delete(gargaMatrix, indColumnRemove, 1)
		#drop position
		gargaPos = np.delete(gargaPos, indColumnRemove, 0)

	#Replace na values by 0
	if switchNA :
		gargaMatrix = np.where(gargaMatrix == -1, 0, gargaMatrix)
		
	#save gargammel data as npz
	np.savez_compressed(output_dir + "/data_post_gargammel.npz", SNP = gargaMatrix, POS = gargaPos)
	
	"""
	#Create distance File
	
	#Comparaison
	data = np.load(originalData)
	SimulationMatrix = data["SNP"]
	Simulationpos = data["POS"]


	nbdif, nber,tabdif,taber = dis.distance (SimulationMatrix, gargaMatrix , Simulationpos, gargaPos, length)
	gargaSing = dis.singleton_number(gargaMatrix)
	simuSing = dis.singleton_number(SimulationMatrix)
	
	#output this result in file
	with open(output_dir + "/distance.txt", "w+") as file:
		file.write("difference number : " + str(nbdif) + "\n")
		file.write("missing data number : " + str(nber) + "\n")
		file.write("singleton number in the simulation matrix : " + str(simuSing) + "\n")
		file.write("singleton number in the gargammel matrix : " + str(gargaSing) + "\n")
	"""