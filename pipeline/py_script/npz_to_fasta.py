#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 9:49:22 2020

@author: mathieu
"""
import numpy as np
import utils
import os
import sys

def convert_npz_to_fasta(directory, length, filename, R, absolutePos, ploidy):
	#Extract the matrix and the position list from the input file
	simu, pos = utils.read_ms_compressed(filename)
	#SNP positions , relative ?
	if not absolutePos :
		pos = (pos* length).astype(int)

	#extends the sequence to avoid mapping errors at the extremities
	pos = pos + R
	length = length + 2 * R
	
	#if the sequence length is in the SNP positions, manage conflict
	if length in pos:
		pos[np.where(pos == length)] = length - 1

	#Generate random sequence
	seq_anc = np.random.choice(["A","T","C","G"], int(length))
	
	#Store random generated sequence in a file
	refdir = directory + "/reference"
	if os.path.exists(refdir):
		with open(refdir + "/reference.fa", "w") as refFile:
			refFile.write(">reference\n")
			refFile.write(utils.concatenate_list(seq_anc))
	else:
		print("you must create reference directory to execute script")
	#Create base matrix
	aln_anc = np.array([seq_anc.tolist()]*len(simu))
	
	#Add SNP to the matrix 
	SNP = utils.searchSNP(pos, simu, seq_anc)
	for nuc in SNP.keys():
		aln_anc[SNP[nuc]['x'], SNP[nuc]['y']] = nuc
	
	#Create the repository which will contain all the fasta files
	fastasDir = directory +  "/fastas"
	
	# range(aln_anc.shape[0])
	#file_writing(fastasDir, aln_anc, range(aln_anc.shape[0]))

	if os.path.exists(fastasDir):
		#Create a repertory for each individual
		if ploidy == 1:
			for row in range(aln_anc.shape[0]) :
				individuRepertory = fastasDir + "/individual_" + str(row)
				#Create directory
				os.mkdir(individuRepertory)
				#Create endo, bact, cont directory
				os.mkdir(individuRepertory + "/endo")
				os.mkdir(individuRepertory + "/bact")
				os.mkdir(individuRepertory + "/cont")
				#Write the indidual file
				fastaFile = individuRepertory  + "/endo/individual" + str(row) + ".1.fa"
				with open(fastaFile, 'w') as file:
					file.write(">individual" + str(row) +"\n")
					file.write(utils.concatenate_list(aln_anc[row]))
					file.close()
					
		else : #ploidy==2
			if aln_anc.shape[0] % 2 != 0:
				raise ValueError("odd matrix given for diploid individuals")
			for row in range(0, aln_anc.shape[0], 2) :
				individuRepertory = fastasDir + "/individual_" + str(row)
				#Create directory
				os.mkdir(individuRepertory)
				#Create endo, bact, cont directory
				os.mkdir(individuRepertory + "/endo")
				os.mkdir(individuRepertory + "/bact")
				os.mkdir(individuRepertory + "/cont")
				#Write the indidual file
				fastaFile1 = individuRepertory  + "/endo/individual" + str(row) + ".1.fa"
				with open(fastaFile1, 'w') as file:
					file.write(">individual" + str(row) +"\n")
					file.write(utils.concatenate_list(aln_anc[row]))
					file.close()
				fastaFile2 = individuRepertory  + "/endo/individual" + str(row) + ".2.fa"
				with open(fastaFile2, 'w') as file:
					file.write(">individual" + str(row) +"\n")
					file.write(utils.concatenate_list(aln_anc[row + 1]))
					file.close()
			
	else:
		print("you must create fastas directory to execute script")
 
#Execute program
if __name__ == '__main__':
	directory = sys.argv[1]
	filename = sys.argv[2]
	length = int(sys.argv[3])
	R = int(sys.argv[4])
	absolutePos = bool(int(sys.argv[5]))
	ploidy = int(sys.argv[6])
	
	convert_npz_to_fasta(directory, length, filename, R, absolutePos, ploidy)