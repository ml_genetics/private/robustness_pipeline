#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 16:04:42 2020

@author: mathieu
"""
import numpy as np

#trunc at certain number of decimals
def trunc(values, decs=0) :
	return np.trunc(values*10**decs)/(10**decs)

#Compute distance
def distance (SimulationMatrix, gargaMatrix , Simulationpos, gargaPos, length):
	#nombre de 0 dans length
	size = len(str(length)) -1
	Simulationpos = trunc(Simulationpos, size)
	
	#Create matrix of the same size with same positions
	#to do : try with Pandas
	posConc = np.concatenate((Simulationpos, gargaPos))
	uniqPosConc = np.unique(posConc)
	
	nbPos = len(uniqPosConc)
	nbInd = len(SimulationMatrix[:,1])
	
	fullSimulationMatrix = np.empty((nbInd ,nbPos), dtype = int)
	fullGargaMatrix = np.empty((nbInd ,nbPos), dtype = int)
	
	for i, pos in enumerate(uniqPosConc) :
		indSimu = np.where(Simulationpos == pos)
		if indSimu[0].size == 0:
			fullSimulationMatrix[:,i] = np.zeros(nbInd)
		else :
			fullSimulationMatrix[:,i] = SimulationMatrix[:, indSimu[0][0]]
	
		indGarga = np.where(gargaPos == pos)
		if indGarga[0].size == 0:
			fullGargaMatrix[:,i] = np.zeros(nbInd)
		else :
			fullGargaMatrix[:,i] = gargaMatrix[:, indGarga[0][0]]
	
	nbdif = 0
	nber = 0
	tabdif = []
	taber = []
	

	for ix,iy in np.ndindex(fullSimulationMatrix.shape):
		if fullSimulationMatrix[ix,iy] == 0 and fullGargaMatrix[ix,iy] == 1 or \
			fullSimulationMatrix[ix,iy] == 1 and fullGargaMatrix[ix,iy] == 0 :
				nbdif += 1
				tabdif.append(uniqPosConc[iy])
		if fullGargaMatrix[ix,iy] == -1:
			nber += 1
			taber.append(uniqPosConc[iy])
	return nbdif, nber, tabdif, taber

def singleton_number(mat):
	singCount = 0
	for col in range(mat.shape[1]) :
		digit, count  = np.unique(mat[:,col], return_counts=True)
		counts =  dict(zip(digit, count))
		for k in counts.keys() :
			if k != -1 and counts[k] == 1:
				singCount += 1
	return singCount
