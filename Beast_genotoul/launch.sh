#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=6G
#SBATCH --array=0-500

module load bioinfo/snakemake-5.8.1
module load system/Java8
module load bioinfo/beast-v2.5.1
module load system/R-3.6.2
module load system/Python-3.7.4
module load compiler/gcc-7.2.0

dir=$1
echo "Starting task $SLURM_ARRAY_TASK_ID"
list=($(ls $dir))
rep=${list[${SLURM_ARRAY_TASK_ID}]}
filename=$(echo "$rep" | cut -f 1 -d '.')

outputpath=/home/mmichel/work/beastgen/out/"$filename"
mkdir -p "$outputpath"
srun snakemake --cores 1 --config in="$dir" plot=FALSE conf=/home/mmichel/work/beastgen/config.yaml read=infile out="$outputpath" ext=.npz burnin=0.1 name=$filename