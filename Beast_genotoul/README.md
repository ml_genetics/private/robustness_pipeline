# # Execute Beast on Genotoul
Dependencies :

- R libraries :
Babette which use :
beautier, beastier, mauricer, tracerer

For the moment these librairies are not available on genotoul. So we have to export their files on the cluster and use ".libPaths("path to your library") " at the begining of each R script

- All the other dependencies are on genotoul take look at the "module load" on launch.sh 


## Launch.sh
To execute the beast pipeline on the different replicates we are using the launch.sh  script.

It takes in argument a directory containing the list of the scenarios, in order to execute the beast pipeline on each replicates of each scenarios

You also have to change the outputpath variable corresponding to the location where the results will be stored

sbatch option :
#SBATCH --mem-per-cpu=6G
During my test, I figure out that the pipeline crash with less ram (it may be caused by the fact that I used two million base pair sequences)
#SBATCH --array=0-1000%500
Use Slurm job array with 1000 tasks but only 500 can be execute at the same time, so it will treat 







