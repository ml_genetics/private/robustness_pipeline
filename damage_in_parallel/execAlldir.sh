#!/bin/bash

#current directory
basedir="/home/mmichel/work/launch"
#result directory
resdir="/home/mmichel/saveresult/launch"
distribfile="/home/mmichel/work/launch/highdistribution.txt"
mapdamagefile="/home/mmichel/work/launch/highmisincorporation.txt"
scenarfiles="/home/mmichel/work/launch/scenarios"
mkdir -p "$resdir"

#list containing the pipeline options for each different execution
list="tc_2,ts_t13 tc_4,ts_t10 tc_8"
#-c 2 -s -13 / -c 4 -s -10 / -c 8
for dir in $list
do
	"$basedir"/exec.sh "$dir" "$resdir" "$distribfile" "$mapdamagefile" "$scenarfiles" "$basedir" &
done