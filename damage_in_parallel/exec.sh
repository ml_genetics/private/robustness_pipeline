#!/bin/bash

dom=$1
resdir=$2
distribfile=$3
mapdamagefile=$4
scenarfiles=$5
basedir=$6

command=`echo "$dom" | tr t "-" | tr _ " "`
arr=(${command//,/ })


for scenar in "$scenarfiles"/*
do
	scenarName=$(basename -- "$scenar")
	respath="$resdir"/"$dom"/"$scenarName"
	mkdir -p "$respath"
	for rep in $scenar/*
	do
		repName=$(basename -- "$rep")
		
		sbatch --wait "$basedir/pipeline.sh" "${arr[@]}" -z -f "$rep" -l 2000000 -F "$distribfile" -m "$mapdamagefile" -n 0.04  -g "$basedir" -o "$respath" -w "$resdir"/"$dom"/work
		#rename the cretaed npz
		mv "$respath"/data_post_gargammel.npz "$respath"/B"$repName"
	done
done
